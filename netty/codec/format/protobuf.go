/*
 * Copyright 2019 the go-netty project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package format

import (
	"encoding/binary"
	"fmt"
	"github.com/golang/protobuf/proto"
	"sync"
	"serialnet/app"
	"serialnet/netty"
	"serialnet/netty/codec"
	"serialnet/netty/utils"
	"serialnet/protocol"
)

const PackLength = 2048
const ExSize = 30

// scheme :0 -> tcp; !=0 -> udp
func ProtobufCodec(scheme uint32, maxFrameLength uint32) codec.Codec {
	utils.AssertIf(maxFrameLength <= 0, "maxFrameLength must be a positive integer")
	return &protobufCodec{
		scheme:         scheme,
		maxFrameLength: maxFrameLength,
		frameLength:    0,
		buffers:        sync.Pool{New: func() interface{} { return new([PackLength]byte) }},
	}
}

type protobufCodec struct {
	scheme         uint32
	buffer         []byte
	frameLength    uint32
	maxFrameLength uint32
	buffers        sync.Pool
}

func (v *protobufCodec) CodecName() string {
	return "protobuf-codec"
}
func (v *protobufCodec) HandleRead(ctx netty.InboundContext, message netty.Message) {
	reader := utils.MustToReader(message)
	data := v.buffers.Get().(*[PackLength]byte) // make([]byte, v.maxFrameLength)
	defer func() {
		v.buffers.Put(data)
	}()
	rn, err := reader.Read(data[:])
	if rn <= 0 && err != nil {
		return
	}
	// udp
	if v.scheme != 0 {
		frameLength, num := utils.Uvarint32(data[:])
		if num == 0 && rn < 5 || frameLength > v.maxFrameLength { // 不完整帧头，或过大
			return
		}
		if num >= rn-2 {
			return
		}
		if data[num] == 144 && data[num+1] == 3 {
			msg := &protocol.MsgDataFrame{}
			if err := proto.Unmarshal(data[num:rn], msg); err == nil {
				app.RuntimeService.SetStats(uint64(ExSize+len(msg.Data)), true, msg.Token == 0)
				if len(msg.Data) > 0 {
					dst := v.buffers.Get().(*[PackLength]byte)
					if l, e := app.RuntimeService.DecryptMsg(msg, dst[:]); e == nil {
						msg.Data = dst[:l]
					}
					ctx.HandleRead(msg)
					v.buffers.Put(dst)
				} else {
					ctx.HandleRead(msg)
				}
			}
		} else {
			msg := &protocol.MsgServerFrame{}
			if err := proto.Unmarshal(data[num:rn], msg); err == nil {
				app.RuntimeService.SetStats(uint64(msg.XXX_Size()), true, false)
				ctx.HandleRead(msg)
			}
		}
	} else { // tcp
		if v.buffer == nil || len(v.buffer) == 0 {
			v.buffer = data[:rn]
		} else {
			v.buffer = append(v.buffer, data[:rn]...)
		}
		v.readAndDeal(ctx)
	}

}

func (v *protobufCodec) readAndDeal(ctx netty.InboundContext) {
	defer func() {
		if err := recover(); err != nil {
			v.frameLength = 0
			v.buffer = nil
		}
	}()
	var num int
	var frameLength uint32
	if v.frameLength == 0 {
		frameLength, num = utils.Uvarint32(v.buffer)
		if num == 0 && len(v.buffer) < 5 || frameLength > 2*1024*1024 { // 不完整帧头，或过大
			v.frameLength = 0
			return
		}
		utils.AssertIf(num < 0, "n < 0: value larger than 64 bits")
		v.frameLength = frameLength
		v.buffer = v.buffer[num:]
	} else {
		frameLength = v.frameLength
		num = 0
	}

	if len(v.buffer) == int(frameLength) {
		data := v.buffer
		v.buffer = nil
		v.frameLength = 0
		if data[0] == 144 && data[1] == 3 { // field index 区分
			msg := &protocol.MsgDataFrame{}
			if err := proto.Unmarshal(data, msg); err == nil {
				app.RuntimeService.SetStats(uint64(ExSize+len(msg.Data)), true, msg.Token == 0)
				if len(msg.Data) > 0 {
					dst := v.buffers.Get().(*[PackLength]byte)
					if l, e := app.RuntimeService.DecryptMsg(msg, dst[:]); e == nil {
						msg.Data = dst[:l]
					}
					ctx.HandleRead(msg)
					v.buffers.Put(dst)
				} else {
					ctx.HandleRead(msg)
				}
			}
		} else {
			msg := &protocol.MsgServerFrame{}
			if err := proto.Unmarshal(data, msg); err == nil {
				app.RuntimeService.SetStats(uint64(msg.XXX_Size()), true, false)
				ctx.HandleRead(msg)
			}
		}
		return
	} else if len(v.buffer) > int(frameLength) {
		for len(v.buffer) > int(frameLength) {
			data := v.buffer[:frameLength]
			v.buffer = v.buffer[frameLength:]
			if data[0] == 144 && data[1] == 3 {
				msg := &protocol.MsgDataFrame{}
				if err := proto.Unmarshal(data, msg); err == nil {
					app.RuntimeService.SetStats(uint64(ExSize+len(msg.Data)), true, msg.Token == 0)
					if len(msg.Data) > 0 {
						dst := v.buffers.Get().(*[PackLength]byte)
						if l, e := app.RuntimeService.DecryptMsg(msg, dst[:]); e == nil {
							msg.Data = dst[:l]
						}
						ctx.HandleRead(msg)
						v.buffers.Put(dst)
					} else {
						ctx.HandleRead(msg)
					}
				}
			} else {
				msg := &protocol.MsgServerFrame{}
				if err := proto.Unmarshal(data, msg); err == nil {
					app.RuntimeService.SetStats(uint64(msg.XXX_Size()), true, false)
					ctx.HandleRead(msg)
				}
			}
			frameLength, num = utils.Uvarint32(v.buffer)
			if num == 0 && len(v.buffer) < 5 || frameLength > 2*1024*1024 { // 不完整帧头，最大2M
				v.frameLength = 0
				return
			}
			utils.AssertIf(num < 0, "n < 0: value larger than 64 bits")
			v.frameLength = frameLength
			v.buffer = v.buffer[num:]
		}
		if len(v.buffer) == int(frameLength) {
			data := v.buffer
			v.buffer = nil
			v.frameLength = 0
			if data[0] == 144 && data[1] == 3 {
				msg := &protocol.MsgDataFrame{}
				if err := proto.Unmarshal(data, msg); err == nil {
					app.RuntimeService.SetStats(uint64(ExSize+len(msg.Data)), true, msg.Token == 0)
					if len(msg.Data) > 0 {
						dst := v.buffers.Get().(*[PackLength]byte)
						if l, e := app.RuntimeService.DecryptMsg(msg, dst[:]); e == nil {
							msg.Data = dst[:l]
						}
						ctx.HandleRead(msg)
						v.buffers.Put(dst)
					} else {
						ctx.HandleRead(msg)
					}
				}
			} else {
				msg := &protocol.MsgServerFrame{}
				if err := proto.Unmarshal(data, msg); err == nil {
					app.RuntimeService.SetStats(uint64(msg.XXX_Size()), true, false)
					ctx.HandleRead(msg)
				}
			}
			return
		} else if len(v.buffer) < int(frameLength) {
			return
		}
	} else {
	}
}

func (v *protobufCodec) HandleWrite(ctx netty.OutboundContext, message netty.Message) {
	if message == nil {
		return
	}
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(fmt.Sprintf("msg encode failed:%s", err))
		}
	}()
	switch m := message.(type) {
	case *protocol.MsgDataFrame:
		switch m.MsgType {
		case protocol.MsgType_Msg_Packet:
			if len(m.Data) > 0 {
				dst := v.buffers.Get().(*[PackLength]byte)
				defer v.buffers.Put(dst)
				if l, e := app.RuntimeService.EncryptMsg(m, dst[:]); e == nil {
					m.Data = dst[:l]
				}
			}
			app.RuntimeService.SetStats(uint64(ExSize+len(m.Data)), false, m.Token == 0)
			break
		default:
			app.RuntimeService.SetStats(uint64(m.XXX_Size()), false, m.Token == 0)
		}
		data, err := proto.Marshal(m)
		if err != nil {
			return
		}
		// encode header
		var head = [binary.MaxVarintLen32]byte{}
		n := utils.PutUvarint32(head[:], uint32(len(data)))

		// optimize one merge operation to reduce memory allocation.
		ctx.HandleWrite([][]byte{
			// header
			head[:n],
			// payload
			data,
		})
	case *protocol.MsgPeerFrame:
		app.RuntimeService.SetStats(uint64(m.XXX_Size()), false, m.Token == 0)
		data, err := proto.Marshal(m)
		if err != nil {
			return
		}
		// encode header
		var head = [binary.MaxVarintLen32]byte{}
		n := utils.PutUvarint32(head[:], uint32(len(data)))

		// optimize one merge operation to reduce memory allocation.
		ctx.HandleWrite([][]byte{
			// header
			head[:n],
			// payload
			data,
		})
	default:
		bodyBytes := utils.MustToBytes(message)
		// encode header
		var head = [binary.MaxVarintLen32]byte{}
		n := utils.PutUvarint32(head[:], uint32(len(bodyBytes)))

		// optimize one merge operation to reduce memory allocation.
		ctx.HandleWrite([][]byte{
			// header
			head[:n],
			// payload
			bodyBytes,
		})
		return
	}
}
