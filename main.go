package main

import (
	"fmt"
	"os"
	"serialnet/app"
	"serialnet/common"
	"serialnet/config"
	"serialnet/model"
	"serialnet/service"
)

func main() {
	fmt.Println("当前程序版本:", app.Version)

	// APP配置
	app.Logger = common.NewLogger(true, true, model.Error)
	if err := config.LoadAppConfig(); err != nil {
		app.Logger.Error("App Config Load Failed,App Run With Default Config.")
	} else {
		app.Logger = common.NewLogger(app.Config.EnableLog, app.Config.EnableLog, app.Config.LogLevel)
		app.Logger.Info("App Config Load Success")
	}

	// Runtime
	app.RuntimeService = service.NewRuntimeServer()
	if err := app.RuntimeService.Start(); err != nil {
		app.Logger.Error("App Runtime Service Start Failed:", err.Error())
		return
	}
	app.Logger.Info("App Runtime Service Start Success")

	signalChan := make(chan os.Signal, 1)
	signal := <-signalChan // 进程退出

	_ = app.RuntimeService.Stop()

	app.Logger.Info("App Exit With :%s", signal.String())
}
