module serialnet

go 1.18

require (
	github.com/digineo/go-uci v0.0.0-20210918132103-37c7b10c14fa
	github.com/golang/protobuf v1.5.2
	golang.org/x/sys v0.0.0-20220627191245-f75cf1eec38b
)

require google.golang.org/protobuf v1.26.0 // indirect
