package common

import (
	"math/rand"
	"os"
	"path/filepath"
	"runtime"
	"time"
)

var osType = runtime.GOOS

func GenRandom(n int) string {
	letters := []byte("0123456789abcdefghijklmnopqrstuvwxyz")
	buffer := make([]byte, n)

	size := len(letters)
	rand.Seed(time.Now().UnixNano())
	for i := range buffer {
		buffer[i] = letters[rand.Int63()%int64(size)]
	}
	return string(buffer)
}

func GetProjectPath() string {
	var projectPath string
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		projectPath, _ = os.Getwd()
	} else {
		projectPath = dir
	}
	return projectPath
}

func GetConfigPath() string {
	path := GetProjectPath()
	if osType == "windows" {
		path = path + "\\" + "config\\"
	} else if osType == "linux" {
		path = path + "/" + "config/"
	}
	return path
}

func GetStoreFilePath() string {
	path := GetProjectPath()
	if osType == "windows" {
		path = path + "\\" + "file\\"
	} else if osType == "linux" {
		path = path + "/" + "file/"
	}
	return path
}

func GetLogPath() string {
	path := GetProjectPath()
	if osType == "windows" {
		path = path + "\\log\\"
	} else if osType == "linux" {
		path = path + "/log/"
	}
	return path
}
