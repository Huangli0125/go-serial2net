package model

type LogType byte

const (
	Error LogType = 0
	Warn  LogType = 1
	Info  LogType = 2
	Debug LogType = 3
)
