package model

type MsgType uint8

// message type
//#define TCP_COMM_TRAN_MSG           0 // c <-> s
//#define TCP_COMM_QUERY_PARA         1 // c  -> s
//#define TCP_COMM_QUERY_PARA_ACK     2 // s  -> c
//#define TCP_COMM_SET_PARA           3 // c  -> s
//#define TCP_COMM_SET_PARA_ACK       4 // s  -> c
//#define TCP_COMM_TRAN_CTRL          5 // c  -> s
//#define TCP_COMM_TRAN_CTRL_ACK      6 // s  -> c

const (
	MsgTypeTransMsg     MsgType = 0 // c <-> s
	MsgTypeQueryPara    MsgType = 1 // c  -> s
	MsgTypeQueryParaAck MsgType = 2 // s  -> c
	MsgTypeSetPara      MsgType = 3 // c  -> s
	MsgTypeSetParaAck   MsgType = 4 // s  -> c
	MsgTypeTransCtrl    MsgType = 5 // c  -> s
	MsgTypeTransCtrlAck MsgType = 6 // s  -> c
)
const ClientStarter uint8 = 0xAA // 来自客户端的头
const ServerStarter uint8 = 0xBB // 服务器发出的头
const Ender uint8 = 0xEE

type MsgFrame struct {
	Start  uint8
	Func   MsgType
	Length uint16
	Data   []byte
	Crc    uint16
	End    uint8
}
