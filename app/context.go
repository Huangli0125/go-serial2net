package app

import (
	"serialnet/common"
	"serialnet/iface"
	"serialnet/model"
)

var Version = "1.001.20220628"

var Config *model.AppConfig

var Logger *common.Logger // 全局日志

var RuntimeService iface.RuntimeInterface
