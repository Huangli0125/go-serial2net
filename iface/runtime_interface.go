package iface

import (
	"serialnet/model"
	"serialnet/netty"
)

type RuntimeInterface interface {
	Start() error
	Stop() error
	SetHandler(isRs232 bool, handler netty.HandlerContext)
	ProcessMessage(isRs232 bool, msg *model.MsgFrame) error
}
