GO := go
CFLAGS := -I/program/openwrt-water/staging_dir/target-mipsel_24kc_musl/usr/include
LDFLAGS := -L/program/openwrt-water/staging_dir/target-mipsel_24kc_musl/usr/lib -ldl -pthread -lrt
ENV := GOOS=linux GOARCH=mipsle GOMIPS=softfloat  CC=mipsel-openwrt-linux-gcc CGO_ENABLED=1 CGO_CFLAGS="$(CFLAGS)" CGO_LDFLAGS="$(LDFLAGS)"
OBJECT=serial2net
all: fmt  build
fmt:
	$(ENV) $(GO) fmt ./...
build: $(OBJECT)
$(OBJECT):
	$(ENV) $(GO) build -ldflags '-w -s' -o ./build/$(OBJECT)  ./ && upx ./build/$(OBJECT) && cp  ./build/$(OBJECT) ../openwrt-n2n/files/usr/serial2net/
clean:
	-rm  ./build/$(OBJECT)

